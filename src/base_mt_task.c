/* based_mt_task.c -- A basic multi-threaded real-time task skeleton. 
 *
 * This (by itself useless) task demos how to setup a multi-threaded LITMUS^RT
 * real-time task. Familiarity with the single threaded example (base_task.c)
 * is assumed.
 *
 * Currently, liblitmus still lacks automated support for real-time
 * tasks, but internaly it is thread-safe, and thus can be used together
 * with pthreads.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Include gettid() */
#include <sys/types.h>

/* Include threading support. */
#include <pthread.h>

/* Include the LITMUS^RT API.*/
#include "litmus.h"

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avconfig.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <SDL2/include/SDL.h>
#include <SDL2/include/SDL_thread.h>


#define PERIOD            34
#define RELATIVE_DEADLINE 34
#define EXEC_COST         15

/* Let's create 10 threads in the example, 
 * for a total utilization of 1.
 */
#define NUM_THREADS      2 

/* The information passed to each thread. Could be anything. */
struct thread_context {
	int id;
};

/* The real-time thread program. Doesn't have to be the same for
 * all threads. Here, we only have one that will invoke job().
 */

/*
* Initializes decoder thread
*/
void* rt_thread_decoder(void *tcontext);

/*
* Initializes display thread
*/
void* rt_thread_display(void *tcontext);

/* Declare the periodically invoked job. 
 * Returns 1 -> task should exit.
 *         0 -> task should continue.
 */


int displayJob(void);

int decodingJob(void);

void cleanupFFmpeg(void);

void cleanupSDL(void);

void setupDecoder();

void setupDisplay(void);


AVFormatContext *pFormatCtx = NULL;
AVCodecContext *pCodecCtx = NULL;
AVCodec *pCodec = NULL;
int j, videoStream= -1;
SDL_Event event;
SDL_Window *screen;
SDL_Renderer *renderer;
SDL_Texture *texture;
Uint8 *yPlane, *uPlane, *vPlane;
size_t yPlaneSz, uvPlaneSz;
int uvPitch;
struct SwsContext *swsCtx = NULL;
int frameFinished;
AVPacket packet;
AVFrame *pFrame;
AVFrame picture;
pthread_mutex_t bufferLock;
pthread_mutex_t initializationLock;
int isBufferFrameRead = 1;
int isComplete = 0;
SDL_Event sdlEvent;
char *filePath;

/* Catch errors.
 */
#define CALL( exp ) do { \
		int ret; \
		ret = exp; \
		if (ret != 0) \
			fprintf(stderr, "%s failed: %m\n", #exp);\
		else \
			fprintf(stderr, "%s ok.\n", #exp); \
	} while (0)


/* Basic setup is the same as in the single-threaded example. However, 
 * we do some thread initiliazation first before invoking the job.
 */
int main(int argc, char** argv)
{
	int i;
	struct thread_context ctx[NUM_THREADS];
	pthread_t             task[NUM_THREADS];
	filePath = argv[1];
	
	/* The task is in background mode upon startup. */		


	/*****
	 * 1) Command line paramter parsing would be done here.
	 */


       
	/*****
	 * 2) Work environment (e.g., global data structures, file data, etc.) would
	 *    be setup here.
	 */



	/*****
	 * 3) Initialize LITMUS^RT.
	 *    Task parameters will be specified per thread.
	 */
	init_litmus();

	if(pthread_mutex_init(&bufferLock, NULL) != 0) {
		fprintf(stderr,"Buffer Mutex initialization failed. Exiting the program");
		exit(1);
	}

	if(pthread_mutex_init(&initializationLock, NULL) != 0) {
		fprintf(stderr,"Initialization Mutex failed. Exiting the program");
		exit(1);
	}

	/***** 
	 * 4) Launch threads.
	 */
	ctx[0].id = 0;
	ctx[1].id = 1;
	pthread_create(task + 0, NULL, rt_thread_decoder, (void *) (ctx + 0));
	pthread_create(task + 1, NULL, rt_thread_display, (void *) (ctx + 1));

	
	/*****
	 * 5) Wait for RT threads to terminate.
	 */
	for (i = 0; i < NUM_THREADS; i++)
		pthread_join(task[i], NULL);
	

	/***** 
	 * 6) Clean up, maybe print results and stats, and exit.
	 */
	return 0;
}



/* A real-time thread is very similar to the main function of a single-threaded
 * real-time app. Notice, that init_rt_thread() is called to initialized per-thread
 * data structures of the LITMUS^RT user space libary.
 */
void* rt_thread_decoder(void *tcontext)
{
	int do_exit;
	struct thread_context *ctx = (struct thread_context *) tcontext;
	struct rt_task param;

	/* Set up task parameters */
	init_rt_task_param(&param);
	param.exec_cost = ms2ns(EXEC_COST);
	param.period = ms2ns(PERIOD);
	param.relative_deadline = ms2ns(RELATIVE_DEADLINE);

	/* What to do in the case of budget overruns? */
	param.budget_policy = NO_ENFORCEMENT;

	/* The task class parameter is ignored by most plugins. */
	param.cls = RT_CLASS_SOFT;

	/* The priority parameter is only used by fixed-priority plugins. */
	param.priority = LITMUS_LOWEST_PRIORITY;

	/* Make presence visible. */
	printf("\nRT Thread %d for decoding the video is active.\n", ctx->id);

	/*****
	 * 1) Initialize real-time settings.
	 */
	CALL( init_rt_thread() );

	/* To specify a partition, do
	 *
	 * param.cpu = CPU;
	 * be_migrate_to(CPU);
	 *
	 * where CPU ranges from 0 to "Number of CPUs" - 1 before calling
	 * set_rt_task_param().
	 */
	CALL( set_rt_task_param(gettid(), &param) );

	/*****
	 * 2) Transition to real-time mode.
	 */
	CALL( task_mode(LITMUS_RT_TASK) );

	/* The task is now executing as a real-time task if the call didn't fail. 
	 */

	pthread_mutex_lock(&initializationLock);
	setupDecoder();
	pthread_mutex_unlock(&initializationLock);
	sleep_next_period();
	/*****
	 * 3) Invoke real-time jobs.
	 */
	do {
		/* Wait until the next job is released. */
		sleep_next_period();
		/* Invoke job. */
		do_exit = decodingJob();		
	} while (!do_exit);


	
	/*****
	 * 4) Transition to background mode.
	 */
	CALL( task_mode(BACKGROUND_TASK) );
	
	cleanupFFmpeg();

	return NULL;
}

void* rt_thread_display(void *tcontext)
{
	int do_exit;
	struct thread_context *ctx = (struct thread_context *) tcontext;
	struct rt_task param;

	/* Set up task parameters */
	init_rt_task_param(&param);
	param.exec_cost = ms2ns(EXEC_COST);
	param.period = ms2ns(PERIOD);
	param.relative_deadline = ms2ns(RELATIVE_DEADLINE);

	/* What to do in the case of budget overruns? */
	param.budget_policy = NO_ENFORCEMENT;

	/* The task class parameter is ignored by most plugins. */
	param.cls = RT_CLASS_SOFT;

	/* The priority parameter is only used by fixed-priority plugins. */
	param.priority = LITMUS_LOWEST_PRIORITY;

	/* Make presence visible. */
	printf("\nRT Thread %d for displaying the video is active.\n", ctx->id);

	/*****
	 * 1) Initialize real-time settings.
	 */
	CALL( init_rt_thread() );

	/* To specify a partition, do
	 *
	 * param.cpu = CPU;
	 * be_migrate_to(CPU);
	 *
	 * where CPU ranges from 0 to "Number of CPUs" - 1 before calling
	 * set_rt_task_param().
	 */
	CALL( set_rt_task_param(gettid(), &param) );

	/*****
	 * 2) Transition to real-time mode.
	 */
	CALL( task_mode(LITMUS_RT_TASK) );

	/* The task is now executing as a real-time task if the call didn't fail. 
	 */

	pthread_mutex_lock(&initializationLock);
	setupDisplay();
	pthread_mutex_unlock(&initializationLock);
	sleep_next_period();
	
	/*****
	 * 3) Invoke real-time jobs.
	 */
	do {
		/* Wait until the next job is released. */
		sleep_next_period();
		/* Invoke job. */
		do_exit = displayJob();		
	} while (!do_exit);


	
	/*****
	 * 4) Transition to background mode.
	 */
	CALL( task_mode(BACKGROUND_TASK) );

	cleanupSDL();

	return NULL;
}

int decodingJob(void) 
{
	pthread_mutex_lock(&bufferLock);
	if(!isBufferFrameRead) {
		if(frameFinished==1) {
			pthread_mutex_unlock(&bufferLock);
			return 0;
		}
	}

	if(av_read_frame(pFormatCtx, &packet) >= 0) {
           //Check if the packet is from the video stream
           if(packet.stream_index == videoStream) {
                //Decode video frame
                avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
	        pthread_mutex_unlock(&bufferLock);
		isBufferFrameRead=0;
		return 0;
			
	   }
	} else {
		isComplete = 1;
		pthread_mutex_unlock(&bufferLock);
		return 1;
	}
	pthread_mutex_unlock(&bufferLock);
	return 0;
}



int displayJob(void) 
{
	pthread_mutex_lock(&bufferLock);
	
	//Was the frame read        
 	if(frameFinished && !isBufferFrameRead) {
		picture.data[0] = yPlane;
                picture.data[1] = uPlane;
                picture.data[2] = vPlane;

                picture.linesize[0] = pCodecCtx->width;
                picture.linesize[1] = uvPitch;
                picture.linesize[2] = uvPitch;

                //Convert the image into YUV format for SDL to use
                sws_scale(swsCtx, (uint8_t const * const *)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, picture.data, picture.linesize);
                                SDL_UpdateYUVTexture(
                        texture,
                        NULL,
                        yPlane,
                        pCodecCtx->width,
                        uPlane,
                        uvPitch,
                        vPlane,
                        uvPitch
                    );
		
		SDL_RenderClear(renderer);
                SDL_RenderCopy(renderer, texture, NULL, NULL);
                SDL_RenderPresent(renderer);
		isBufferFrameRead=1;
	}
        pthread_mutex_unlock(&bufferLock); 
        
	if(SDL_PollEvent(&event)) {
		if(event.type == SDL_QUIT){
			fprintf(stderr, "\nTerminate call received from SDL screen. Exiting.\n");
			exit(1);
		}

	}  

	if(isComplete)
		return 1;
	else 
		return 0;
}

/*
* 1) Initialize FFmpeg variables
* 2) Open the video file and get th`e stream info
* 3) Determine the decoder required
* 4) Open the appropriate codec
*/
void setupDecoder() {
        av_register_all();

        fprintf(stderr, "\nOpen video file at the location: %s", filePath);
        //Open the video file and set the context
        if(avformat_open_input(&pFormatCtx, filePath, NULL, NULL) != 0){
                fprintf(stderr,"\nUnable to open the video file from the path: %s",filePath);
                exit(1);
        }

        fprintf(stderr, "\nSuccessfully openend the video file at location: %s", filePath);

        //Get stream info
        if(avformat_find_stream_info(pFormatCtx, NULL) <0) {
                fprintf(stderr,"\nUnable to get the stream info");
                exit(1);
        }

        fprintf(stderr, "\nObtained stream info");

        //Find and use the first stream
        for(j=0; j<pFormatCtx->nb_streams; j++)
                if(pFormatCtx->streams[j]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
                        videoStream=j;
                        break;
                }
        if(videoStream == -1) {
                fprintf(stderr,"\nUnable to find the video stream in the given stream");
                exit(1);
        }

        //Get pointer to the codec context for video stream
        pCodecCtx = pFormatCtx->streams[videoStream]->codec;
                                                                   
        //Find the decoder for the video stream
        pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
        if(pCodec == NULL) {
                fprintf(stderr,"\nUnable to decode. Unsupported codec");
                exit(1);
        }

        //Is this bbufferLock really needed? Check!
/*
        //Copy context
        pCodecCtx = avcodec_alloc_context3(pCodec);
        if(avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0) {
         //Log error. Cannot copy codec
         fprintf(stderr,"\nUnable to copy the codec");
         fflush(stderr);
         return -1;
        }
*/
        //Open codec
        if(avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
                fprintf(stderr,"\nUnable to open the codec");
                exit(1);
        }
	
	fprintf(stderr, "\nDecoder setup successful\n");

        pFrame = av_frame_alloc();
}


/*
* 1) Initialize SDL
* 2) Create a display
* 3) Create a renderer
* 4) Initialize frame
*/
void setupDisplay() {
        //Initialize SDL
        if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
          fprintf(stderr, "\nCould not initialize SDL - %s\n", SDL_GetError());
          exit(1);
        }

        //Create a display
        screen = SDL_CreateWindow("VideoApp - LITMUS-RT", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,pCodecCtx->width, pCodecCtx->height, 0);

        if(!screen) {
          fprintf(stderr, "\nSDL: failed to set video mode. Exiting");
          exit(1);
	}

        //Create a renderer
        renderer = SDL_CreateRenderer(screen, -1, 0);
        if (!renderer) {
          fprintf(stderr, "SDL: could not create renderer - exiting\n");
          exit(1);
        }


        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_YV12, SDL_TEXTUREACCESS_STREAMING, pCodecCtx->width, pCodecCtx->height);
        if (!texture) {
          fprintf(stderr, "SDL: could not create texture - exiting\n");
          exit(1);
        }


        //Initialize SWS context
        swsCtx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_YUV420P, SWS_BILINEAR, NULL, NULL, NULL);

        // set up YV12 pixel array (12 bits per pixel)
        yPlaneSz = pCodecCtx->width * pCodecCtx->height;
        uvPlaneSz = pCodecCtx->width * pCodecCtx->height / 4;
        yPlane = (Uint8*)malloc(yPlaneSz);
        uPlane = (Uint8*)malloc(uvPlaneSz);
        vPlane = (Uint8*)malloc(uvPlaneSz);

        if (!yPlane || !uPlane || !vPlane) {
          fprintf(stderr, "Could not allocate pixel buffers - exiting\n");
          exit(1);
        }

        uvPitch = pCodecCtx->width / 2;
        j=0;
	fprintf(stderr, "\nDisplay setup successful\n");
}


void cleanupFFmpeg() {
         //Free the memory allocated for the packet
         av_packet_unref(&packet);

        //Free the memory allocated for the picture
        av_frame_unref(&picture);

        //Close the codec & input
        avcodec_close(pCodecCtx);
        avformat_close_input(&pFormatCtx);      
}

void cleanupSDL() {
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(screen);
        SDL_Quit();     
}

